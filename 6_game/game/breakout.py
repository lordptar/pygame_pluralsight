#!/usr/bin/env python3
import pygame

from game import Level
from game import Pad
from game import Ball
from game import HighScore
from game.scenes import Scene
from game.scenes import MenuScene
from game.scenes import PlayingGameScene
from game.scenes import HighScoreScene
from game.scenes import GameOverScene
from game.shared import GameConstants


class Breakout:

    def __init__(self):
        self.__lives = 5
        self.__score = 0
        self.__level = Level(self)
        self.__level.load(0)
        self.__pad = Pad(
                            (GameConstants.SCREEN_SIZE[0]/2,
                             GameConstants.SCREEN_SIZE[1] - GameConstants.PAD_SIZE[1]),
                            pygame.image.load(GameConstants.SPRITE_PAD),
                         )
        # probably better load image in GameObject class
        # why is here list?
        self.__balls = [
                        Ball(
                             (0, 500),
                             pygame.image.load(GameConstants.SPRITE_BALL),
                             self
                             )
                        ]

        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption("Superb brick's breakout game")
        self.__clock = pygame.time.Clock()
        # self.screen = pygame.display.set_mode(GameConstants.SCREEN_SIZE,
        #                                       pygame.DOUBLEBUF | pygame.FULLSCREEN,

        self.screen = pygame.display.set_mode(GameConstants.SCREEN_SIZE,
                                              pygame.DOUBLEBUF,
                                              32)

        pygame.mouse.set_visible(0)
        self.__scenes = (
                         PlayingGameScene(self),
                         GameOverScene(self),
                         HighScoreScene(self),
                         MenuScene(self)
                         )
        self.__current_scene = 0
        self.__sounds = ()

    def start(self):
        while True:
            self.__clock.tick(100)
            self.screen.fill((0, 0, 0))

            current_scene = self.__scenes[self.__current_scene]
            current_scene.handle_events(pygame.event.get())
            current_scene.render()
            pygame.display.update()

    def change_scene(self, scene):
        self.__current_scene = scene

    def get_level(self):
        return self.__level

    def get_score(self):
        return self.__score

    def increase_score(self, score):
        self.__score += score

    def get_lives(self):
        return self.__lives

    def get_balls(self):
        return self.__balls

    def get_pad(self):
        return self.__pad

    def play_sound(self, sound_clip):
        sound = self.__sounds[sound_clip]
        sound.stop()
        sound.play()

    def reduce_lives(self):
        self.__lives -= 1

    def increase_lives(self):
        self._lives += 1

    def reset(self):
        pass


if __name__ == '__main__':
    breakout = Breakout()
    breakout.start()
