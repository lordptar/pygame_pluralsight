from game.bricks import Brick


class SpeedBrick(Brick):
    def __init__(self, position, sprite, game):
        super().__init__(position, sprite, game)
