import pygame
from game.shared import GameObject
from game.shared import GameConstants


class Pad(GameObject):

    def __init__(self, position, sprite):
        super().__init__(position, GameConstants.PAD_SIZE, sprite)
        self.__y = GameConstants.SCREEN_SIZE[1] - GameConstants.PAD_SIZE[1]

    def update_position(self):
        x = pygame.mouse.get_pos()[0]
        if x + GameConstants.PAD_SIZE[0] >= GameConstants.SCREEN_SIZE[0]:
            x = GameConstants.SCREEN_SIZE[0] - GameConstants.PAD_SIZE[0]
        self.set_position([x,
                          self.__y])
