class GameObject:

    def __init__(self, position, size, sprite):
        self.__position = position
        self.__size = size
        self.__sprite = sprite

    def set_position(self, position):
        self.__position = position

    def get_position(self):
        return self.__position

    def get_size(self):
        return self.__size

    def get_sprite(self):
        return self.__sprite

    def __intersect_x(self, other):
        object_x = self.__position[0]
        object_width = self.__size[0]
        other_x = other.get_position()[0]
        other_width = other.get_size()[0]
        if object_x >= other_x and object_x <= (other_x + other_width):
            return 1
        if (object_x + object_width) >= other_x and (object_x + object_width) <= (other_x + other_width):
            return 1
        else:
            return 0

    def __intersect_y(self, other):
        object_y = self.__position[1]
        object_height = self.__size[1]
        other_y = other.get_position()[1]
        other_height = other.get_size()[1]
        if object_y >= other_y and object_y <= (other_y + other_height):
            return 1
        if (object_y + object_height) >= other_y and (object_y + object_height) <= (other_y + other_height):
            return 1
        else:
            return 0

    def intersects(self, other):
        if self.__intersect_x(other) and self.__intersect_y(other):
            return 1
        else:
            return 0
