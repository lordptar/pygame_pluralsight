import os


class GameConstants:
    # the object size you can get from sprite
    # sprite = pygame.image.load("sprite_path")
    # sprite_size = sprite.get_size()
    BRICK_SIZE = [100, 30]
    BALL_SIZE = [16, 16]
    PAD_SIZE = [139, 13]
    SCREEN_SIZE = [800, 600]

    SPRITE_BALL = os.path.join("assets",
                               "ball.png")
    SPRITE_PAD = os.path.join("assets",
                              "pad.png")
    SPRITE_BRICK = os.path.join("assets",
                                "standard_brick.png")
    SPRITE_SPEED_BRICK = os.path.join("assets",
                                      "speed_brick.png")
    SPRITE_LIFE_BRICK = os.path.join("assets",
                                     "life_brick.png")
