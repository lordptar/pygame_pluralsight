from game.scenes.scene import Scene
from game.scenes.playing_game_scene import PlayingGameScene
from game.scenes.game_over_scene import GameOverScene
from game.scenes.high_score_scene import HighScoreScene
from game.scenes.menu_scene import MenuScene
