import pygame
from game.scenes.scene import Scene


class PlayingGameScene(Scene):

    def __init__(self, game):
        super().__init__(game)
        self.intersects = 0

    def render(self):
        super().render()

        game = self.get_game()

        balls = game.get_balls()
        pad = game.get_pad()
        # if we have several ball and they collide with each another
        for ball in balls:
            for ball2 in balls:
                if ball != ball2 and ball.intersects(ball2):
                    ball.change_direction(ball2)
            for brick in game.get_level().get_bricks():
                if not brick.is_destroyed() and ball.intersects(brick):
                        self.intersects += 1
                        brick.hit()
                        # in future passing intersecting side(x and y)
                        ball.change_direction(brick)
                        break
            if ball.intersects(pad):
                ball.change_direction(pad)

            ball.update_position()
            game.screen.blit(ball.get_sprite(), ball.get_position())

        for brick in game.get_level().get_bricks():
            if not brick.is_destroyed():
                game.screen.blit(brick.get_sprite(),
                                 brick.get_position()
                                 )
        pad.update_position()
        game.screen.blit(
                         pad.get_sprite(),
                         pad.get_position()
                         )

    def handle_events(self, events):
        super().handle_events(events)

        for event in events:
            if event.type == pygame.QUIT:
                exit()
