#!/usr/bin/env python3
import os
from PIL import Image


def extract_frames(inGif, outFolder):
    frame = Image.open(inGif)
    nframes = 0
    while frame:
        frame.save('%s/%s-%s.gif' % (outFolder, os.path.basename(inGif), nframes ), 'GIF')
        nframes += 1
        try:
            frame.seek(nframes)
        except EOFError:
            break;
    return True

extract_frames("mario_hummer.gif", "mario_hummer")
