#!/usr/bin/env python3
# base on exemple https://www.youtube.com/watch?v=vIwpvkkvos0&t=5s

import pygame
import sys
import glob
import re

_nsre = re.compile('([0-9]+)')


def natural_sort_key(s):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]


width = 800
hight = 600

screen = pygame.display.set_mode((width, hight))

clock = pygame.time.Clock()


class Player:
    def __init__(self, images_path, animation_speed=1, x=0, y=0):
        self.x = x
        self.y = y
        self.animation_speed = animation_speed
        self.animation_speed_counter = self.animation_speed
        self.animation_images = glob.glob(images_path)
        self.animation_images.sort(key=natural_sort_key)
        self.animation_image_number = 0  # the image number in the image's list
        self.animation_max_image = len(self.animation_images) - 1
        self.image = pygame.image.load(self.animation_images[0])
        self.update(0)

    def update(self, lets_move):
        if lets_move != 0:
            self.animation_speed_counter -= 1
            self.x += lets_move
            if self.animation_speed_counter == 0:
                if lets_move == 1:
                    if self.animation_image_number == self.animation_max_image:
                        self.animation_image_number = 0
                    else:
                        self.animation_image_number += 1
                if lets_move == -1:
                    if self.animation_image_number == 0:
                        self.animation_image_number = self.animation_max_image
                    else:
                        self.animation_image_number -= 1
                self.image = pygame.image.load(self.animation_images[self.animation_image_number])
                self.animation_speed_counter = self.animation_speed

        screen.blit(self.image, (self.x, self.y))


player_1 = Player("mario_walk/*.gif", 2)
lets_move = 0


while 1:
    screen.fill((0, 0, 0))
    clock.tick(90)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            lets_move = 1
        elif event.type == pygame.KEYUP and event.key == pygame.K_RIGHT:
            lets_move = 0
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            lets_move = -1
        elif event.type == pygame.KEYUP and event.key == pygame.K_LEFT:
            lets_move = 0
    player_1.update(lets_move)
    pygame.display.update()
