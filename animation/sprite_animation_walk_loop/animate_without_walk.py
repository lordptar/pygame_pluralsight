#!/usr/bin/env python3
# based on example htts://www.youtube.com/watch?v=vIwpvkkvos0&t=5s
import pygame
import sys
import glob
import re

_nsre = re.compile('([0-9]+)')


def natural_sort_key(s):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]


width = 800
hight = 600

screen = pygame.display.set_mode((width, hight))

clock = pygame.time.Clock()


class Player:
    def __init__(self, image_path, animation_speed=1, x=0, y=0):
        self.image_path = image_path
        self.x = x
        self.y = y
        self.animation_speed = animation_speed
        self.animation_speed_counter = self.animation_speed
        self.animation_images = glob.glob(self.image_path)
        self.animation_images.sort(key=natural_sort_key)
        print(self.animation_images)
        self.animation_image_number = 0  # the image number in list
        self.animation_max_image = len(self.animation_images) - 1
        self.image = pygame.image.load(self.animation_images[0])
        self.update()

    def update(self):
        self.animation_speed_counter -= 1
        if self.animation_speed_counter == 0:
            self.animation_speed_counter = self.animation_speed
            self.image = pygame.image.load(self.animation_images[self.animation_image_number])
            if self.animation_image_number == self.animation_max_image:
                self.animation_image_number = 0
            else:
                self.animation_image_number += 1

        screen.blit(self.image, (self.x, self.y))


player_1 = Player("mario_walk/*.gif", 2)


while 1:
    screen.fill((0, 0, 0))
    clock.tick(30)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    player_1.update()
    pygame.display.update()
