#!/usr/bin/env python3
# test1_pyganim.py - A very very very basic pyganim test program.
#
# This program just runs a single animation. It shows you what you need to do to use Pyganim. Basically:
#   1) Import the pyganim module
#   2) Create a pyganim.PygAnimation object, passing the constructor a list of image filenames and durations.
#   3) Call the play() method.
#   4) Call the blit() method.
#
# The animation images come from POW Studios, and are available under an Attribution-only license.
# Check them out, they're really nice.
# http://powstudios.com/

import pygame
import sys
import time
import pyganim
import glob
from pygame.locals import *

pygame.init()

# set up the window
windowSurface = pygame.display.set_mode((800, 600), 0, 32)
pygame.display.set_caption('Pyganim Test 1')
img_names = glob.glob("mario_walk/*.gif")
img_names.sort()
img_time = [(i, 0.1) for i in img_names]
# create the animation objects   ('filename of image',    duration_in_seconds)
boltAnim = pyganim.PygAnimation(img_time)
boltAnim.play() # there is also a pause() and stop() method

mainClock = pygame.time.Clock()
BGCOLOR = (100, 50, 50)
while True:
    windowSurface.fill(BGCOLOR)
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN and event.key == K_l:
            # press "L" key to stop looping
            boltAnim.loop = False

    boltAnim.blit(windowSurface, (100, 50))

    pygame.display.update()
    mainClock.tick(60) # Feel free to experiment with any FPS setting.
