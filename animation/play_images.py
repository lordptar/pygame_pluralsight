import glob
import pygame
import sys

pygame.init()
pygame.mixer.init()
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

img_names = glob.glob("pooh/*.gif")
all_imgs = {}
clock = pygame.time.Clock()

while 1:
    clock.tick(60)
    # screen.fill((0, 0, 0))  # fill screen with black

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    for img in img_names:
        all_imgs[img] = pygame.image.load(img)

    for img in img_names:
        screen.blit(all_imgs[img], (0, 0))

    pygame.display.update()
