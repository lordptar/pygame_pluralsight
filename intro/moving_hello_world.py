#!/usr/bin/env python3

import pygame
import sys

pygame.init()

# create the window
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

# create a text
myriad_pro_font = pygame.font.SysFont('Myriad Pro', 48)
hello_world = myriad_pro_font.render('Hello World',
                                     1,
                                     (255, 0, 255),  # text colour
                                     (255, 255, 255))  # background colour
hello_world_size = hello_world.get_size()

# start coordinates of text 'Hello World'
x, y = 0, 0
direction_x = 1
direction_y = 1
clock = pygame.time.Clock()

while True:

    clock.tick(40)  # code will run 40 times per second

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill((0, 0, 0,))  # fill screen with black
    screen.blit(hello_world,  # test
                (x, y))  # coordinates of text
    x += 5 * direction_x
    y += 5 * direction_y

    if x + hello_world_size[0] > 800 or x <= 0:
        direction_x *= -1
    if y + hello_world_size[1] > 600 or y <= 0:
        direction_y *= -1

    pygame.display.update()
