#!/usr/bin/env python3

import pygame
import sys

pygame.init()

# create the window
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

# create a text
myriad_pro_font = pygame.font.SysFont('Myriad Pro', 48)
hello_world = myriad_pro_font.render('Hello World',
                                     1,
                                     (255, 0, 255),  # text colour
                                     (255, 255, 255))  # background colour
hello_world_size = hello_world.get_size()

# start coordinates of text 'Hello World'
x, y = 0, 0
direction_x = 1
direction_y = 1
clock = pygame.time.Clock()

while True:

    clock.tick(40)  # code will run 40 times per second

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill((0, 0, 0,))  # fill screen with black

    mouse_position = pygame.mouse.get_pos()

    x, y = mouse_position

    # we have to handle just right and bottom boundaries of screen for mouse.
    # because you can't have less than zero mouse coordinates
    if x + hello_world_size[0] > 800:
        x = 800 - hello_world_size[0]

    if y + hello_world_size[1] > 600:
        y = 600 - hello_world_size[1]

    screen.blit(hello_world,  # test
                (x, y))  # coordinates of text

    pygame.display.update()
