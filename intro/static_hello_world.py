#!/usr/bin/env python3

import pygame
import sys

pygame.init()

# create the window
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

# create a text
myriad_pro_font = pygame.font.SysFont('Myriad Pro', 48)
hello_world = myriad_pro_font.render('Hello World',
                                     1,
                                     (255, 0, 255),  # text colour
                                     (255, 255, 255))  # background colour

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.blit(hello_world,  # test
                (0, 0))  # coordinates of text
    pygame.display.update()
