#!/usr/bin/env python3

import pygame
import sys

pygame.init()
pygame.mixer.init()

# create the window
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

# create a text
cat_image = pygame.image.load('cat.jpeg')
cat_image_size = cat_image.get_size()

# make mouse invisible
pygame.mouse.set_visible(0)

# start coordinates of text 'Hello World'
x, y = 0, 0
direction_x = 1
direction_y = 1
clock = pygame.time.Clock()

while True:

    clock.tick(40)  # code will run 40 times per second

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    screen.fill((0, 0, 0,))  # fill screen with black

    mouse_position = pygame.mouse.get_pos()

    x, y = mouse_position

    # we have to handle just right and bottom boundaries of screen for mouse.
    # because you can't have less than zero mouse coordinates
    if x + cat_image_size[0] > 800:
        x = 800 - cat_image_size[0]
        if not pygame.mixer.music.get_busy():
            music = pygame.mixer.music.load('cat_meow_x.wav')
            pygame.mixer.music.play()

    if y + cat_image_size[1] > 600:
        y = 600 - cat_image_size[1]
        if not pygame.mixer.music.get_busy():
            music = pygame.mixer.music.load('cat_y.wav')
            pygame.mixer.music.play()

    if y <= 0 or x <= 0:
        if not pygame.mixer.music.get_busy():
            music = pygame.mixer.music.load('cat_big.wav')
            pygame.mixer.music.play()

    if pygame.mixer.music.get_busy():
        print(" ... music is playing")
    else:
        print(" ... music is not playing")

    screen.blit(cat_image,  # test
                (x, y))  # coordinates of text

    pygame.display.update()
