#!/usr/bin/env python3

import pygame
import sys
from GameObject import GameObject


# Initialize Pygame
pygame.init()
pygame.mixer.init()
window_size = (800, 600)
screen = pygame.display.set_mode(window_size)

# Load resources
image = pygame.image.load("jim4.png")
# image = pygame.image.load("circle.png")
sound = pygame.mixer.Sound("groovy.wav")
myriad_pro_font = pygame.font.SysFont("Myriad Pro", 48)
intersect_text = myriad_pro_font.render("Intersecting!",
                                        1,
                                        (255, 0, 255),  # text colour
                                        (0, 0, 0)  # background colour
                                        )

# Prepare logo
image_size = image.get_size()
# image.fill((50, 50, 0), None, pygame.BLEND_RGBA_MAX)
image.fill((0, 0, 0), None, pygame.BLEND_RGBA_MAX)

x, y = 0, 0
direction_x,  direction_y = 1, 1
clock = pygame.time.Clock()


def play_sound():
    sound.stop()
    sound.play()


rectangle_size = (200,  # x start point for drawing
                  200,  # y start point for drawing
                  200,  # x side size
                  200)  # y side size
rectangle = GameObject(rectangle_size)
logo = GameObject((0, 0, image_size[0], image_size[1]))


while 1:
    clock.tick(30)
    screen.fill((0, 0, 0))  # fill screen with black

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    mouse_position = pygame.mouse.get_pos()

    x = mouse_position[0]
    y = mouse_position[1]
    logo.set_position(x,
                      y)

    if logo.intersects(rectangle):
        screen.blit(intersect_text, (10, 10))  # draw static text
        play_sound()

    if x + image_size[0] > 800:
        x = 800 - image_size[0]

    if y + image_size[1] > 600:
        y = 600 - image_size[1]

    if y < 0:
        y = 0

    if x < 0:
        x < 0

    pygame.draw.rect(screen,
                     (255, 255, 255),  # rectangle colour
                     rectangle_size,
                     1)
    screen.blit(image, (x, y))
    pygame.display.update()
