from Game.Scenes.Scene import Scene
from Game.Scenes.PlayingGameScene import PlayGameScene
from Game.Scenes.MenuScene import MenuScene
from Game.Scenes.HighscoreScene import HighScoreScene
from Game.Scenes.GameOverScene import GameOverScene
