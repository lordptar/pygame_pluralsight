class First:
    def __init__(self):
        pass

    def super_method(self, phrase="hi from first class"):
        print("#" * 10)
        print("Class First, super_method")
        print("#" * 10)
        print("This method from first class")
        print(phrase)
        print("-" * 10)


class Second(First):
    def __init__(self):
        pass

    def super_method(self, phrase="hi from second class", extra="test"):
        print("#" * 10)
        print("Class Second, super_method")
        print("#" * 10)
        super().super_method(phrase)
        print("This method from second class")
        print(phrase)
        print("print something additional")
        print("-" * 10)

    def super_method_2(self, phrase="hi from 2 second class", extra="test"):
        print("#" * 10)
        print("Class Second, super_method_2")
        print("#" * 10)
        super().super_method(phrase)
        print("This method_2 from second class")
        print(phrase)
        print("print something additional 2")
        print("-" * 10)


second = Second()
second.super_method("hi from second method")
second.super_method_2("hi from 2")
