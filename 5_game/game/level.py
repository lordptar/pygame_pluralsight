import os
import fileinput
import pygame
from game.shared import GameObject
from game.shared import GameConstants
from game.bricks import Brick
from game.bricks import LifeBrick
from game.bricks import SpeedBrick


class Level:

    def __init__(self, game):
        self.__game = game
        self.__bricks = []
        self.__amount_of_bricks_left = 0
        self.__current_level = 0

    def get_bricks(self):
        return self.__bricks

    def get_amount_of_bricks_left(self):
        return self.__amount_of_bricks_left

    def brick_hit(self):
        self.__amount_of_bricks_left -= 1

    def load_next_level(self):
        pass

    def load(self, level):
        self.__current_level = level
        self.__bricks = []

        x, y = 0, 0
        level_file_name = "level{}.dat".format(level)
        level_file_path = os.path.join("assets",
                                       "levels",
                                       level_file_name
                                       )
        for line in fileinput.input(level_file_path):
            for current_brick in line:
                if current_brick == "1":
                    brick = Brick(
                                  [x, y],
                                  pygame.image.load(GameConstants.SPRITE_BRICK),
                                  self.__game
                                  )
                    self.__bricks.append(brick)
                    self.__amount_of_bricks_left += 1
                elif current_brick == "2":
                    brick = SpeedBrick(
                                       [x, y],
                                       pygame.image.load(GameConstants.SPRITE_SPEED_BRICK),
                                       self.__game
                                       )
                    self.__bricks.append(brick)
                    self.__amount_of_bricks_left += 1
                elif current_brick == "3":
                    brick = LifeBrick(
                                      [x, y],
                                      pygame.image.load(GameConstants.SPRITE_LIFE_BRICK),
                                      self.__game
                                      )
                    self.__bricks.append(brick)
                    self.__amount_of_bricks_left += 1

                x += GameConstants.BRICK_SIZE[0]

            x = 0
            y += GameConstants.BRICK_SIZE[1]
