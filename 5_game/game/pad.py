from game.shared import GameObject
from game.shared import GameConstants


class Pad(GameObject):

    def __init__(self, position, sprite):
        super().__init__(position, GameConstants.PAD_SIZE, sprite)
